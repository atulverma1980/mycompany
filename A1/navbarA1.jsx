import React from "react";
import {Link} from "react-router-dom";
export default class Navbar extends React.Component{

    render(){
        return(<nav className="navbar navbar-expand navbar-dark bg-dark">
                 <Link to="/" className="navbar-brand">
                    MyCompany
                </Link>
                <div className="">
                    <ul className="navbar-nav mr-auto">
                    <li className="nav-item">
                            <Link className="nav-link" to={`/emps`}>
                            All   
                            </Link>
                        </li>
                        {this.props.location.map((loc)=>(
                        <li className="nav-item">
                            <Link className="nav-link" to={`/emps/${loc}`}>
                             {loc}   
                            </Link>
                        </li>
                        ))}
                    </ul>
                </div>
        </nav>);
    }
}