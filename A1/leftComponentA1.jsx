import React,{Component} from "react";
class LeftComponent extends Component{
    handleChange=(e)=>{
        const{currentTarget:input}=e;
        let option ={...this.props.option};
 input.name=="department"?option[input.name]=this.updateCBs(option[input.name],input.checked,input.value):option[input.name]=input.value;     
 this.props.onOptionChange(option);
    }
    updateCBs = (inpVal,checked,value)=>{
        let inpArr = inpVal?inpVal.split(","):[];
        if(checked)inpArr.push(value);
        else{
            let index = inpArr.findIndex(v=>v===value);
            if(index>=0) inpArr.splice(index,1); 
        }
        return inpArr.join(",");
    }
    makeCB=(arr,values,name,label)=>(
        <React.Fragment>
            <label className="form-check-label font-weight-bold">{label}</label>
            {arr.map(v=><div className="form-check" key={v}>
                <input className="form-check-input" value={v} type="checkbox" name={name}
                checked={values.findIndex(val=>val===v)>=0}onChange={this.handleChange}/>
                <label className="form-check-label">{v}</label>
            </div>)}
        </React.Fragment>
    )
    makeRadio=(arr,values,name,label)=>(
        <React.Fragment>
               <label className="form-check-label font-weight-bold">{label}</label>
            {arr.map(v=><div className="form-check" key={v}>
                <input className="form-check-input" value={v} type="radio" name={name}
                checked={values==v} onChange={this.handleChange}/>
                <label className="form-check-label">{v}</label>
            </div>)}
        </React.Fragment>
    )
    render(){
    const{allOption}=this.props;
    const{designation,department=""}=this.props.option;
    return<div className="row bg-danger">
        <div className="col-12">
            {this.makeRadio(allOption.designation,designation,"designation","Designation")}
            {this.makeCB(allOption.department,department.split(","),"department","Department")}
        </div>
    </div>
    }
}
export default LeftComponent;