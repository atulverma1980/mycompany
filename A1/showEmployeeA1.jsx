import React from "react";
import queryString from "query-string";
import LeftComponent from "./leftComponentA1";
export default class ShowEmp extends React.Component{
    filterParams=(arr,queryParams)=>{
        let{designation,department}=queryParams;
        arr = this.filterParam(arr,"designation",designation);
        arr = this.filterParam(arr,"department",department);
        return arr;
    }
    filterParam=(arr,name,value)=>{
        if(!value)return arr;
        let valueArr = value.split(",");
        let arr1 = arr.filter(a=>valueArr.find(v=>v===a[name]));
        return arr1;
    }
    handlePage=(incr)=>{
        const queryParams = queryString.parse(this.props.location.search);
        let {page="1"}=queryParams;
        let newPage = +page+incr;
        queryParams.page = newPage;
        let {value}=this.props.match.params;
        value?this.callURL(`/emps/${value}`,queryParams):this.callURL(`/emps`,queryParams);

    }
    handleChange=(option)=>{
        option.page="1";
        let {value}=this.props.match.params;
        value?this.callURL(`/emps/${value}`,option):this.callURL(`/emps`,option);

    }
    callURL=(url,option)=>{
    let searchStr=this.makeSearchString(option);
    this.props.history.push({
        pathname:url,
        search:searchStr
    });
    }
    makeSearchString=(option)=>{
        let {page,designation,department}=option;
        let searchStr="";
        searchStr = this.addtoQuery(searchStr,"designation",designation);
        searchStr = this.addtoQuery(searchStr,"department",department);
        searchStr = this.addtoQuery(searchStr,"page",page);
        return searchStr;
    }
    addtoQuery=(str,paramname,paramVal)=>
    paramVal?str?`${str}&${paramname}=${paramVal}`:`${paramname}=${paramVal}`:str;
    makeAllOption = (arr)=>{
        let json={};
        json.designation=this.getDifferentValues(arr,"designation");
        json.department=this.getDifferentValues(arr,"department");
        return json;
    }
    getDifferentValues=(arr,name)=>
    arr.reduce((acc,curr)=>(acc.find(v=>v===curr[name])?acc:[...acc,curr[name]]),[]);
    render(){
        const{employee}=this.props;
        let {value}=this.props.match.params;
        let queryParams = queryString.parse(this.props.location.search);
        let{page="1",designation,department}=queryParams;
        let pageNum = +page;
        let size =2;
        let employee1 = value?employee.filter(v=>v.location===value):employee;
        let employee2 = this.filterParams(employee1,queryParams);
        let allOption = this.makeAllOption(employee);
        let startIndex = (pageNum-1)*size;
        let endIndex = employee2.length>startIndex+size-1?startIndex+size-1:employee2.length-1;
        let emp = employee2.length>2?employee2.filter((lt,index)=>index>=startIndex && index<=endIndex):employee2;
        console.log(emp);
        return <div className="container-fluid">
            <div className="row">
                <div className="col-3">
                    <LeftComponent allOption={allOption} option={queryParams} onOptionChange={this.handleChange}/>
                </div>
                <div className="col-9 bg-light">
                    <h4 className="text-center">Welcome to Employee Portal</h4>
                    <label className="font-weight-bold">You have Choosen</label><br/>
                    <b>Location :</b> <span className="text-primary">{value?value:"All"}</span><br/>
                    <b>Department : </b><span className="text-primary">{department?department:"All"}</span><br/>
                    <b>Designation : </b><span className="text-primary">{designation?designation:"All"}</span><br/>
                    <br/>
                    <p className="text-danger"><b>The Number Of Employee Matching the Option :</b> {employee2.length}</p>
                    <div className="row">
                      {emp.map((v,index)=><div className="col-6 border bg-success"key={index}>
                        <h6>{v.name}</h6>
                        <b>Email :</b>{v.email}<br/>
                        <b>Mobile  :</b>{v.mobile}<br/>
                        <b>Location : </b>{v.location}<br/>
                        <b>Department :</b> {v.department}<br/>
                        <b>Designation :</b> {v.designation}<br/>
                        <b>Salary :</b> {v.salary}<br/>
                      </div>)}
                    </div>
                    <div className="row">
                        <div className="col-2">
                        {startIndex>0?<button className="btn btn-primary m-2"onClick={()=>this.handlePage(-1)}>Prev</button>:""}
                        </div>
                        <div className="col-8"></div>
                        <div className="col-2">
                        {endIndex<employee2.length-1?<button className="btn btn-primary m-2"onClick={()=>this.handlePage(+1)}>Next</button>:""}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }
} 